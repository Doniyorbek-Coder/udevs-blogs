import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./components/paths/home";
import Post from "./components/paths/post";

export default function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path={"/blogs/:id"} element={<Post />} />
          {/* <Route path={"/create"} element={<CreatePost />} />
          <Route path={"/profile"} element={<Profile />} /> */}
        </Routes>
      </div>
    </Router>
  );
}
