import React from "react";
import BlogList from "./blogList/blogList";

export default function Blog() {
  return (
    <div className={`container`} style={{ marginTop: "170px" }}>
      <BlogList />
    </div>
  );
}
