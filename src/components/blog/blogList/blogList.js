import React, { useState, useEffect } from "react";
import BlogListItem from "./blogListItem/blogListItem";
import styles from "./blogList.module.scss";
import { db } from "../../../firebase";
import { collection, getDocs } from "@firebase/firestore";

export default function BlogList() {
  const [posts, setPosts] = useState([]);
  const postsCollectionRef = collection(db, "posts");

  useEffect(() => {
    getPosts();
  }, []);

  function getPosts() {
    getDocs(postsCollectionRef).then((res) =>
      setPosts(res.docs.map((doc) => ({ ...doc.data(), id: doc.id })))
    );
  }

  return (
    <ul className={styles.blogList}>
      {posts?.map((post) => (
        <BlogListItem key={post.id} id={post.id} post={post} />
      ))}
    </ul>
  );
}
