import { fas, faEye } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Link } from "react-router-dom";
import styles from "./blogListItem.module.scss";
export default function BlogListItem({ post }) {
  return (
    <div className={styles.blogItem}>
      <Link to={"/"}>
        <img className={styles.blogImg} src={post.img} alt="blog-item-img" />
      </Link>
      <div className={styles.blogEvent}>
        <span className={styles.blogDate}>{post.date}</span>
        <FontAwesomeIcon icon={(fas, faEye)} />
        <span className={styles.blogView}>{post.view}</span>
      </div>
      <Link to={"/"}>
        <h3 className={styles.blogTitle}>{post.title}</h3>
      </Link>
    </div>
  );
}
