import React from "react";
import styles from "./footer.module.scss";
import logo from "../../components/images/logo.png";
import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <div className={styles.footer}>
      <div className={`container ${styles.footerContainer}`}>
        <ul className={styles.footerList}>
          <li className={styles.footerItem}>
            <Link className={styles.footerLogo} to={"/"}>
              <img className={styles.logoImg} src={logo} alt="logo img" />
            </Link>
            <p className={styles.footerText}>
              Помощник в публикации статей, журналов. <br /> Список популярных
              международных конференций. <br />
              Всё для студентов и преподавателей.
            </p>
            <p className={styles.footerCopyright}>
              Copyright © 2020. LogoIpsum. All rights reserved.
            </p>
          </li>
          <li className={styles.footerItem}>
            <h4 className={styles.footerTitle}>Ресурсы</h4>
            <ul className={styles.footerNav}>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Статьи
                </Link>
              </li>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Журналы
                </Link>
              </li>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Газеты
                </Link>
              </li>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Диплом
                </Link>
              </li>
            </ul>
          </li>
          <li className={styles.footerItem}>
            <h4 className={styles.footerTitle}>О нас</h4>
            <ul className={styles.footerNav}>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Контакты
                </Link>
              </li>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Помощь
                </Link>
              </li>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Заявки
                </Link>
              </li>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Политика
                </Link>
              </li>
            </ul>
          </li>
          <li className={styles.footerItem}>
            <h4 className={styles.footerTitle}>Помощь</h4>
            <ul className={styles.footerNav}>
              <li className={styles.footerNavItem}>
                <Link className={styles.footerNavLink} to={"/"}>
                  Часто задаваемые вопросы
                </Link>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  );
}
