import React from "react";
import { Link } from "react-router-dom";
import logo from "../../components/images/logo.png";
import styles from "./header.module.scss";
import "../../components/styles/globals.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { fas, faBell } from "@fortawesome/free-solid-svg-icons";

export default function Header() {
  return (
    <div className={styles.header}>
      <div className={`container ${styles.headerContainer}`}>
        <Link className={styles.headerLogo} to={"/"}>
          <img className={styles.logoImg} src={logo} alt="logo img" />
        </Link>
        <ul className={styles.headerList}>
          <li className={styles.headerItem}>
            <Link className={styles.headerLink} to={"/"}>
              Все потоки
            </Link>
          </li>
          <li className={styles.headerItem}>
            <Link className={styles.headerLink} to={"/"}>
              Разработка
            </Link>
          </li>
          <li className={styles.headerItem}>
            <Link className={styles.headerLink} to={"/"}>
              Администрирование
            </Link>
          </li>
          <li className={styles.headerItem}>
            <Link className={styles.headerLink} to={"/"}>
              Дизайн
            </Link>
          </li>
          <li className={styles.headerItem}>
            <Link className={styles.headerLink} to={"/"}>
              Менеджмент
            </Link>
          </li>
          <li className={styles.headerItem}>
            <Link className={styles.headerLink} to={"/"}>
              Маркетинг
            </Link>
          </li>
          <li className={styles.headerItem}>
            <Link className={styles.headerLink} to={"/"}>
              Научпоп
            </Link>
          </li>
        </ul>
        <div className={styles.headerNotification}>
          <FontAwesomeIcon icon={(fas, faBell)} />
          <span className={styles.NotificationCount}>1</span>
        </div>
        <button className={styles.headerBtn}>Войти</button>
      </div>
    </div>
  );
}
