import React from "react";
import Blog from "../blog/blog";
import Footer from "../footer/footer";
import Header from "../header/header";

export default function Home() {
  return (
    <div>
      <Header />
      <Blog />
      <Footer />
    </div>
  );
}
