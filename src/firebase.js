import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCEtnDL1QxV1uMavpKMEt4piP6U3GvJP7M",
  authDomain: "udevs-blog-app-4e8ba.firebaseapp.com",
  projectId: "udevs-blog-app-4e8ba",
  storageBucket: "udevs-blog-app-4e8ba.appspot.com",
  messagingSenderId: "483597298032",
  appId: "1:483597298032:web:369f0bcfc7b75c41d6c7b5",
  measurementId: "${config.measurementId}",
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
